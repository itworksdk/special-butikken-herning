package dk.itworks.special_butikken_herning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import scanner.MessageDialogFragment;
import utils.CheckInternet;
import utils.Constants;
import utils.HideKeyboard;


public class CreateUserActivity extends AppCompatActivity implements MessageDialogFragment.MessageDialogListener {
    //Defining views
    private ScrollView new_user_scrollview;
    private EditText new_user_email;
    private EditText new_user_password;
    private EditText new_user_retype_password;
    private EditText new_user_name;
    private EditText new_user_customer_id;
    private Button new_user_button;
    private ProgressBar new_user_progressBar;

    private ArrayList<EditText> errorEdit;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_create_user);

        //Initiating views
        new_user_scrollview = (ScrollView) findViewById(R.id.new_user_scrollview);
        new_user_email = (EditText) findViewById(R.id.new_user_email);
        new_user_password = (EditText) findViewById(R.id.new_user_password);
        new_user_retype_password = (EditText) findViewById(R.id.new_user_retype_password);
        new_user_name = (EditText) findViewById(R.id.new_user_name);
        new_user_customer_id = (EditText) findViewById(R.id.new_user_customer_id);
        new_user_progressBar = (ProgressBar) findViewById(R.id.new_user_progressBar);
        new_user_button = (Button) findViewById(R.id.new_user_button);


        //Create user when click on button
        new_user_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewUser();
            }
        });

        //If user has rotated the screen, set the already-inserted text. If any
        if (b != null) {
            new_user_email.setText(b.getString(Constants.BUNDLE_CREATE_USER_EMAIL));
            new_user_password.setText(b.getString(Constants.BUNDLE_CREATE_USER_PASS));
            new_user_retype_password.setText(b.getString(Constants.BUNDLE_CREATE_USER_PASS2));
            new_user_name.setText(b.getString(Constants.BUNDLE_CREATE_USER_NAME));
            new_user_customer_id.setText(b.getString(Constants.BUNDLE_CREATE_USER_CUSTOMER_ID));
        }
    }

    private void validateEditText(EditText[] fields) {
        errorEdit = new ArrayList<>();
        String error = getResources().getString(R.string.text_empty);

        for (EditText currentField : fields) {
            if (currentField.getText().toString().trim().length() <= 0) {
                currentField.setError(error);
                currentField.setText("");
                errorEdit.add(currentField);
            }
        }
    }

    private void createNewUser() {
        //Checking all text fields, if they are empty.
        validateEditText(new EditText[]{
                new_user_email,
                new_user_password,
                new_user_retype_password,
                new_user_name,
                new_user_customer_id
        });

        //If any textfields are added to the array. Display the error
        if (errorEdit.size() > 0) {
            for (int i = 0; i < errorEdit.size(); ++i) {
                errorEdit.get(i);
            }
        }
        //If all are filled, proceed
        else {
            //Check if password and re-type password is the same.
            if (new_user_password.getText().toString().trim()
                    .equals(new_user_retype_password.getText().toString().trim())) {

                if (new CheckInternet().isOnline(CreateUserActivity.this))
                    createUser();
                else {
                    String title = getResources().getString(R.string.warning);
                    String message = getResources().getString(R.string.check_to_internet);

                    DialogFragment dialogFragment = MessageDialogFragment.newInstance(title, message, this);
                    dialogFragment.show(getSupportFragmentManager(), "no_internet_dialog");
                }

            } else {
                //Display error if password and re-type password do not match.
                Toast.makeText(CreateUserActivity.this, getResources().getString(R.string.password_matching), Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void createUser() {
        //Display a progressbar
        new_user_scrollview.setVisibility(View.GONE);
        new_user_progressBar.setVisibility(View.VISIBLE);

        //Hide the keyboard
        new HideKeyboard(CreateUserActivity.this).hide(new_user_email);

        String url = Constants.URL_CREATE_USER;
        Ion.with(CreateUserActivity.this)
                .load(url)
                .setTimeout(10000)
                .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS)
                .setBodyParameter(Constants.POST_EMAIL, new_user_email.getText().toString().trim())
                .setBodyParameter(Constants.POST_PASS, new_user_password.getText().toString().trim())
                .setBodyParameter(Constants.TAG_NAME, new_user_name.getText().toString().trim())
                .setBodyParameter(Constants.TAG_CUSTOMER_ID, new_user_customer_id.getText().toString().trim())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {

                            //Getting success message.
                            //IF user does NOT exist, display a success message
                            String success = result.get(Constants.SUCCESS).getAsString();
                            if (success.equals("1")) {
                                String title = getResources().getString(R.string.create_user_dialog_title);
                                String message = getResources().getString(R.string.create_user_dialog_message);

                                DialogFragment fragment = MessageDialogFragment.newInstance(title, message, CreateUserActivity.this);
                                fragment.show(getSupportFragmentManager(), "create_user_dialog");


                            } else {
                                //If user DOES exists display an error
                                new_user_scrollview.setVisibility(View.VISIBLE);
                                new_user_progressBar.setVisibility(View.GONE);

                                new_user_email.setText("");

                                Toast.makeText(
                                        CreateUserActivity.this,
                                        getResources().getString(R.string.user_already_existing),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }

                        } else {
                            new_user_scrollview.setVisibility(View.VISIBLE);
                            new_user_progressBar.setVisibility(View.GONE);

                            e.printStackTrace();
                            Toast.makeText(
                                    CreateUserActivity.this,
                                    getResources().getString(R.string.error_server),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);

        //Save any data in the text fields, when user ROTATES the screen.
        b.putString(Constants.BUNDLE_CREATE_USER_EMAIL, new_user_email.getText().toString());
        b.putString(Constants.BUNDLE_CREATE_USER_PASS, new_user_password.getText().toString());
        b.putString(Constants.BUNDLE_CREATE_USER_PASS2, new_user_retype_password.getText().toString());
        b.putString(Constants.BUNDLE_CREATE_USER_NAME, new_user_name.getText().toString());
        b.putString(Constants.BUNDLE_CREATE_USER_CUSTOMER_ID, new_user_customer_id.getText().toString());

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        //Go to the login screen
        Intent intent = new Intent(CreateUserActivity.this, LoginActivity.class);
        //Save the email text, and display it in the login screen (improving UX)
        intent.putExtra(Constants.INTENT_GET_USER_NAME_AFTER_CREATE_USER, new_user_email.getText().toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        dialog.dismiss();
        finish();
    }
}
