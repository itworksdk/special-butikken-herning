package dk.itworks.special_butikken_herning;

/**
 * SUMMARY
 * Created by Harjit
 * -------------------------
 * <p/>
 * This Activity is the main menu for the app
 * <p/>
 * The app displays 3 tabs where each leads to:
 * - SB's website
 * - Previous orders
 * - Logout
 * <p/>
 * The activity also contains a button in the center leading the user to ScannerActivity
 * <p/>
 * Finally the Activity displays an ADVERT-banner at the buttom.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.Random;

import dk.itworks.special_butikken_herning.structure.Vare;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import scanner.MessageDialogFragment;
import utils.Constants;
import utils.NotificationIconHandler;
import utils.RealmHelper;
import utils.SharedPreferencesHolder;


public class MainActivity extends AppCompatActivity implements MessageDialogFragment.MessageDialogListener {

    //Tab buttons
    private RelativeLayout
            main_activity_website_container_button,
            main_activity_previous_order_container_button,
            main_activity_logout_container_button;

    //Scanner button
    private Button main_activity_scanner_button;

    //Banner
    private LinearLayout advert_banner_container;
    private ImageView main_activity_banner;


    //Defining Realm objects
    private RealmConfiguration realmConfiguration;
    private Realm realm;
    private RealmResults<Vare> results;

    private static Button notifCount;
    private static int mNotifCount = 0;

    private SharedPreferencesHolder sph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initiating views
        main_activity_website_container_button = (RelativeLayout) findViewById(R.id.main_activity_website_container_button);
        main_activity_previous_order_container_button = (RelativeLayout) findViewById(R.id.main_activity_previous_order_container_button);
        main_activity_logout_container_button = (RelativeLayout) findViewById(R.id.main_activity_logout_container_button);
        main_activity_logout_container_button = (RelativeLayout) findViewById(R.id.main_activity_logout_container_button);
        main_activity_scanner_button = (Button) findViewById(R.id.main_activity_scanner_button);
        main_activity_banner = (ImageView) findViewById(R.id.main_activity_banner);
        advert_banner_container = (LinearLayout) findViewById(R.id.advert_banner_container);

        //Initiating realm. Using standard realm config (we do not need to handle custom tables)
        realmConfiguration = new RealmConfiguration.Builder(this).build();
        realm = Realm.getInstance(realmConfiguration);

        sph = new SharedPreferencesHolder(MainActivity.this);
        String username = sph.loadString(Constants.USER_NAME_PREF);

        if(username != null && getSupportActionBar() != null)
            getSupportActionBar().setTitle(username);


    }

    //Method that increases cart value and refreshes the icon.
    private void setCartValue(int count) {
        //Setting value
        mNotifCount = count;

        //refreshing
        supportInvalidateOptionsMenu();
    }

    //Displaying logout dialog
    //The method is an onClick method and is assigned for main_activity_logout_container_button
    public void logout(View view) {
        String title = getResources().getString(R.string.logout);
        String message = getResources().getString(R.string.logout_message);

        //Calling dialog fragment, and inserting title and message.
        DialogFragment logoutDialog = MessageDialogFragment.newInstance(title, message, this);
        logoutDialog.show(getSupportFragmentManager(), "logout_dialogfragment");
    }

    //Leading user to SB's website.
    //The method is an onClick method and is assigned for main_activity_website_container_button
    public void goToSBWebsite(View view) {
        String url = Constants.URL_SB_WEBSITE;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    //Leading user to ScannerActivity
    public void goToScannerActivity(View view) {
        startActivity(new Intent(MainActivity.this, ScannerActivity.class));
    }

    public void goToPreviousOrders(View view) {
        startActivity(new Intent(MainActivity.this, PreviousOrderActivity.class));
    }

    //Method that sets a random banner.
    private void displayRandomAdverts() {

        //Array of all banner-resources
        final int[] adverts = {R.drawable.a3m_banner, R.drawable.bahco_banner, R.drawable.metabo_banner, R.drawable.metabo_banner2};

        //Generating a random number between the size of banner-resources.
        int maxRange = adverts.length;
        int minRange = 0; //0 because first element in array is 0.

        Random rand = new Random();
        final int advert = rand.nextInt(maxRange) - minRange;

        //Setting banner based on the random number.
        int banner = adverts[advert];
        main_activity_banner.setBackgroundResource(banner);
        main_activity_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBannerClick(advert, adverts);
            }
        });
    }

    //Setting onClickListener accordingly to banner
    public void setBannerClick(int advert, int[] allAdverts) {

        if (allAdverts[advert] == allAdverts[0]) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String url = Constants.URL_3M_WEBSITE;
            intent.setData(Uri.parse(url));
            startActivity(intent);
        } else if (allAdverts[advert] == allAdverts[1]) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String url = Constants.URL_BAHCO_WEBSITE;
            intent.setData(Uri.parse(url));
            startActivity(intent);
        } else if (allAdverts[advert] == allAdverts[2]) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String url = Constants.URL_METABO_WEBSITE;
            intent.setData(Uri.parse(url));
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String url = Constants.URL_METABO_WEBSITE;
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //The number of items in the cart are determined from mNotifiCount.
        NotificationIconHandler notificationIconHandler = new NotificationIconHandler(MainActivity.this);

        //Setting up and initiating the cart-icon
        notificationIconHandler.notificationIcon(menu, notifCount, mNotifCount);
        return super.onCreateOptionsMenu(menu);
    }


    //DialogFragment's listener
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        //Finishing all running activities and launching LoginActivity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        //Setting the login value to false. User will go to login screen.
        sph = new SharedPreferencesHolder(MainActivity.this);
        sph.saveBoolean(Constants.LOAD_LOGINSCREEN_ONCE, false);

        //Clearing cart when logging out.
        new RealmHelper(realm).clearVareRealm();

        //Calling intent and start LoginActivity
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        displayRandomAdverts();

        //Selecting all items from Vare, and inserting them into a List
        results = realm.where(Vare.class).findAll();

        //Setting the cart number from the size of results' list.
        mNotifCount = results.size();
        setCartValue(mNotifCount);
    }
}
