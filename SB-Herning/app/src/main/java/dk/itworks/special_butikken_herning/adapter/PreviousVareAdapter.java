package dk.itworks.special_butikken_herning.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import dk.itworks.special_butikken_herning.R;
import utils.Constants;

public class PreviousVareAdapter extends BaseAdapter {

    //Initiating views
    private Context context;
    private ArrayList<HashMap<String, String>> resultArray;
    private LayoutInflater inflater;

    public PreviousVareAdapter(
            Context context,
            ArrayList<HashMap<String, String>> resultArray) {

        //Initiating the values
        this.context = context;
        this.resultArray = resultArray;

        //Initiating LayoutInflater. This inflates the parent-view, and draws the UI for a ListItem.
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return resultArray.size();
    }

    @Override
    public Object getItem(int position) {
        return resultArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.single_item_vare, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TextView titleTv = viewHolder.single_vare_name;
        TextView priceTv = viewHolder.single_vare_price;
        TextView eanTv = viewHolder.single_vare_ean_nr;

        //Getting values from the ArrayList
        HashMap<String, String> vare = resultArray.get(position);
        String title = vare.get(Constants.VARE_NAME);
        String amount = vare.get(Constants.AMOUNT);
        String price = vare.get(Constants.VARE_PRICE);
        String ean = vare.get(Constants.VARE_EAN_NUMBER);

        //Inserting values in the Views.
        titleTv.setText(title);
        priceTv.setText(
                amount + " X " +
                        String.format("%.2f", Double.valueOf(price)) + " DKK");
        eanTv.setText(ean);

        return convertView;
    }


    static class ViewHolder {
        private TextView single_vare_name, single_vare_price, single_vare_ean_nr;

        public ViewHolder(View view) {
            single_vare_name = (TextView) view.findViewById(R.id.single_vare_name);
            single_vare_price = (TextView) view.findViewById(R.id.single_vare_price);
            single_vare_ean_nr = (TextView) view.findViewById(R.id.single_vare_ean_nr);

        }
    }
}
