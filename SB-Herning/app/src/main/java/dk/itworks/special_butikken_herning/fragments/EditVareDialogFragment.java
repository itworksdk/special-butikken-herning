package dk.itworks.special_butikken_herning.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import dk.itworks.special_butikken_herning.R;
import dk.itworks.special_butikken_herning.structure.Vare;
import dk.itworks.special_butikken_herning.structure.VareFieldName;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import utils.Constants;


public class EditVareDialogFragment extends DialogFragment {
    //Declaring views
    private TextView edit_vare_title;
    private EditText edit_vare_quantity;
    private TextView edit_vare_price;
    private Button edit_vare_ok_button;
    private Button edit_vare_delete_button;
    private Button edit_vare_cancel_button;

    //Defining all bundle objects.
    //These are used for calling all info from 1 item, when user clicks on a list-item.
    private int idFromBundle;
    private String idFromBundleDB;
    private String vareNumberBundle;
    private String titleFromBundle;
    private double priceFromBundle;
    private String eanFromBundle;
    private int amountFromBundle;

    //Variable for total price in the dialog
    private double totalPrice;

    //Our trusty realms
    private Realm realm;
    private RealmConfiguration config;

    public static EditVareDialogFragment newInstance() {
        return new EditVareDialogFragment();
    }

    public EditVareDialogFragment() {
        //Default empty fragment-constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initating realm
        config = new RealmConfiguration.Builder(getActivity()).build();
        realm = Realm.getInstance(config);

        //Gets all info from 1 item.
        idFromBundle = getArguments().getInt(Constants.BUNDLE_VARE_ID);
        idFromBundleDB = getArguments().getString(Constants.BUNDLE_VARE_ID_DB);
        vareNumberBundle = getArguments().getString(Constants.BUNDLE_VARE_NUMBER);
        titleFromBundle = getArguments().getString(Constants.BUNDLE_TITLE);
        priceFromBundle = Double.valueOf(getArguments().getString(Constants.BUNDLE_PRICE));
        eanFromBundle = getArguments().getString(Constants.BUNDLE_EAN_NUMBER);
        amountFromBundle = getArguments().getInt(Constants.BUNDLE_VARE_AMOUNT);

        //totalPriceTemp = edit_vare_price.getText().toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //We do not want a title in our DialogFragment. Important to call this before "inflater.inflate..."
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_edit_vare, container, false);
        edit_vare_title = (TextView) view.findViewById(R.id.edit_vare_title);
        edit_vare_price = (TextView) view.findViewById(R.id.edit_vare_price);
        edit_vare_quantity = (EditText) view.findViewById(R.id.edit_vare_quantity);
        edit_vare_ok_button = (Button) view.findViewById(R.id.edit_vare_ok_button);
        edit_vare_cancel_button = (Button) view.findViewById(R.id.edit_vare_cancel_button);

        /**
         Whenever the user makes changes to the quantity of item, the local database will be done.
         **/

        edit_vare_ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    //Convert the quantity form String to double.
                    double quantity = Double.valueOf(edit_vare_quantity.getText().toString());

                    //Check if quantity is more than 1. If less than one, display an error.
                    if (quantity >= 1) {

                        //Check if edittext is empty, and display and error if it is.
                        if (TextUtils.isEmpty(edit_vare_quantity.getText().toString())) {
                            String error = getResources().getString(R.string.text_empty);
                            edit_vare_quantity.setError(error);

                            //If no error proceed
                        } else {

                            //Here we update the local database (Realm) for the specific item.
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {

                                    try {
                                        //Inserting item into Realm.
                                        int amt = Integer.valueOf(edit_vare_quantity.getText().toString());
                                        Vare vare = new Vare();
                                        vare.setVareID(idFromBundle);
                                        vare.setVareIdDatabase(idFromBundleDB + "");
                                        vare.setVareNumber(vareNumberBundle);
                                        vare.setVareName(titleFromBundle);
                                        vare.setVarePrice(priceFromBundle);
                                        vare.setVareEANNumber(eanFromBundle);
                                        vare.setVareAmount(amt);

                                        //Getting the total price by multiplying quantity with item-price.
                                        double totalPrice = amt * priceFromBundle;
                                        vare.setVareTotalPrice(totalPrice);

                                        //Updating database and dismiss the DialogFragment.
                                        realm.copyToRealmOrUpdate(vare);
                                        closeDialog();

                                    } catch (NumberFormatException n) {
                                        //If the user enters a comma/fullstop, display and error and reverse the price to the original
                                        numberComma();
                                        n.printStackTrace();
                                    }
                                }
                            });

                            //Update our items
                            updateItemsRealm();
                        }

                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.comma), Toast.LENGTH_SHORT).show();
                    }
                } catch (NumberFormatException ne) {
                    numberComma();
                }
            }
        });

        edit_vare_delete_button = (Button) view.findViewById(R.id.edit_vare_delete_button);
        edit_vare_delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Removing an item from realm.
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Vare vare = realm.where(Vare.class).equalTo("vareID", idFromBundle).findFirst();
                        vare.removeFromRealm();
                    }
                });

                //Updating total price after deleting.
                updateItemsRealm();

                closeDialog();
            }
        });

        edit_vare_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               closeDialog();
            }
        });

        return view;
    }

    private void numberComma() {
        Toast.makeText(getActivity(), getResources().getString(R.string.comma), Toast.LENGTH_SHORT).show();
        edit_vare_quantity.setText("");
        edit_vare_price.setText(String.format("%.2f", priceFromBundle) + " DKK");
    }

    //Call this method to update the realm, whenever we delete/insert items.
    private void updateItemsRealm() {
        RealmResults<Vare> realmResults = realm.where(Vare.class).findAll();
        TextView t = (TextView) getActivity().findViewById(R.id.vare_total_price);
        double price = realmResults.sum(VareFieldName.vareTotalPrice).doubleValue();
        t.setText(String.format("%.2f", price) + " DKK");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Setting title as item-name
        edit_vare_title.setText(titleFromBundle);

        //Setting price which is quantity x price
        double total = amountFromBundle * priceFromBundle;
        edit_vare_price.setText(String.format("%.2f", total) + " DKK");

        //Setting quantity
        edit_vare_quantity.setText(amountFromBundle + "");
        edit_vare_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                double spinnerValue;

                try {
                    //Update item-price accordingly to quantity.
                    if (!s.toString().isEmpty()) {
                        spinnerValue = Double.valueOf(s.toString());
                        totalPrice = spinnerValue * priceFromBundle;
                        edit_vare_price.setText(String.format("%.2f", totalPrice) + " DKK");
                    } else {
                        edit_vare_price.setText(String.format("%.2f", priceFromBundle) + " DKK");
                    }

                } catch (NumberFormatException n) {
                    edit_vare_quantity.setText("");
                }
            }
        });
    }

    private void closeDialog() {
        getDialog().dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}