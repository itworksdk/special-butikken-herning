package dk.itworks.special_butikken_herning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashMap;

import dk.itworks.special_butikken_herning.adapter.VarePreOrderAdapter;
import scanner.MessageDialogFragment;
import utils.Constants;
import utils.SharedPreferencesHolder;


public class PreviousOrderActivity extends AppCompatActivity {

    //Defining Views
    private ListView previous_orders_listView;
    private ProgressBar previous_orders_progressBar;

    //Defining Adapter
    private ListAdapter listAdapter;

    //Defining SharedPrefernces. We use this to retrieve the userID
    private SharedPreferencesHolder sharedPreferencesHolder;

    private ArrayList<HashMap<String, String>> resultArray;
    private ArrayList<HashMap<String, String>> resultVareArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_orders);

        //Initiating Views
        previous_orders_progressBar = (ProgressBar) findViewById(R.id.previous_orders_progressBar);
        previous_orders_listView = (ListView) findViewById(R.id.previous_orders_listView);

        sharedPreferencesHolder = new SharedPreferencesHolder(PreviousOrderActivity.this);

        retrieveOrders();
    }

    private void retrieveOrders() {
        //Displaying progressBar
        previous_orders_progressBar.setVisibility(View.VISIBLE);
        previous_orders_listView.setVisibility(View.GONE);

        //Initiating arraylist. We are putting all values from JSON into this list, and return it to the Adapter.
        resultArray = new ArrayList<>();
        resultVareArray = new ArrayList<>();

        //Fetching userID from SharedPreferences
        long uid = sharedPreferencesHolder.loadLong(Constants.USER_ID_PREF);

        String url = Constants.URL_PREVIOUS_ORDERS;

        //Ion initiation
        Ion.with(getApplicationContext())
                .load(url)
                .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS)
                .setBodyParameter(Constants.TAG_UID, uid + "")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, final JsonObject result) {
                        if (e == null) {

                            try {

                                //Retrieving the JSONArray from the responds
                                JsonArray resArr = result.get(Constants.TAG_ORDERS).getAsJsonArray();

                                /* The first for-loop gets the order.
                                 * The nested for-loop gets the items for each order.
                                 *
                                 * We will put the orders in 1 array
                                 * And the nested orders will be put in another array.
                                 *
                                 * Then for each item, there is an order ID (put in HashMap)
                                 */

                                for (int i = 0; i < resArr.size(); ++i) {
                                    //Fetching order ID and time
                                    final int oid = resArr.get(i).getAsJsonObject().get(Constants.TAG_OID).getAsInt();
                                    final String time = resArr.get(i).getAsJsonObject().get(Constants.TAG_TIME_STAMP).getAsString();

                                    //Inserting OID and time in a Hashmap. TJSON response in HashMap
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put(Constants.TAG_OID, oid + "");
                                    map.put(Constants.TAG_TIME_STAMP, time);

                                    //Fetching all items pr. order
                                    JsonArray vareArray = resArr.get(i).getAsJsonObject().get(Constants.VARER).getAsJsonArray();

                                    for (int u = 0; u < vareArray.size(); ++u) {
                                        //Getting all items pr. order
                                        String vareName = vareArray.get(u).getAsJsonObject().get(Constants.VARE_NAME).getAsString();
                                        String varePrice = vareArray.get(u).getAsJsonObject().get(Constants.VARE_PRICE).getAsString();
                                        String vareNumber = vareArray.get(u).getAsJsonObject().get(Constants.VARE_NUMBER).getAsString();
                                        String eanNumber = vareArray.get(u).getAsJsonObject().get(Constants.VARE_EAN_NUMBER).getAsString();
                                        String amount = vareArray.get(u).getAsJsonObject().get(Constants.AMOUNT).getAsString();

                                        //Indserting items in Hashmap
                                        HashMap<String, String> vareMap = new HashMap<>();
                                        vareMap.put(Constants.VARE_NAME, vareName);
                                        vareMap.put(Constants.VARE_PRICE, varePrice);
                                        vareMap.put(Constants.VARE_NUMBER, vareNumber);
                                        vareMap.put(Constants.VARE_EAN_NUMBER, eanNumber);
                                        vareMap.put(Constants.AMOUNT, amount);
                                        vareMap.put(Constants.TAG_OID, oid + ""); //Filter the orders by the OID

                                        resultVareArray.add(vareMap);
                                    }

                                    //Inserting HashMap object to resultArray
                                    resultArray.add(map);
                                }


                            } catch (Exception x) {
                                x.printStackTrace();

                                //Something went wrong, display an error
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.general_error), Toast.LENGTH_LONG).show();
                                previous_orders_progressBar.setVisibility(View.GONE);
                                previous_orders_listView.setVisibility(View.VISIBLE);
                            }


                        } else {
                            e.printStackTrace();
                            //Something went wrong, display an error
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.general_error), Toast.LENGTH_LONG).show();
                            previous_orders_progressBar.setVisibility(View.GONE);
                            previous_orders_listView.setVisibility(View.VISIBLE);

                        }

                        //Setting newest orders first
                        //Collections.reverse(resultArray);

                        //Setting the adapter, and inserting resultArray to fill the adapter.
                        listAdapter = new VarePreOrderAdapter(PreviousOrderActivity.this, resultArray);
                        previous_orders_listView.setAdapter(listAdapter);
                        previous_orders_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                HashMap<String, String> map = resultArray.get(position);
                                String oid = map.get(Constants.TAG_OID);

                                //Parsing all items pr. order to the intent, which we will retrieve from the PreviousOrderVareActivity
                                Intent intent = new Intent(PreviousOrderActivity.this, PreviousOrderVareActivity.class);
                                intent.putExtra(Constants.INTENT_PREVIOUS_ORDER_VARER_BUNDLE, resultVareArray);
                                intent.putExtra(Constants.INTENT_PREVIOUS_ORDER_VARER_BUNDLE_OID, oid);
                                startActivity(intent);

                            }
                        });

                        previous_orders_progressBar.setVisibility(View.GONE);
                        previous_orders_listView.setVisibility(View.VISIBLE);

                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_previous_orders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.delete_previous_order_menu_item) {
            //Standard dialogfragment handling
            String title = getResources().getString(R.string.delete_title);
            String addedToCart = getResources().getString(R.string.delete_all_message);

            //Inserting the bundle containing the order ID and launching the dialog
            DialogFragment fragment = MessageDialogFragment.newInstance(title, addedToCart, new MessageDialogFragment.MessageDialogListener() {
                @Override
                public void onDialogPositiveClick(DialogFragment dialog) {
                    deleteAllOrders();
                }
            });
            fragment.show(getSupportFragmentManager(), "delete_all_pre_order_dialog");

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteAllOrders() {
        //Fetching the order ID from the bundle
        long uid = sharedPreferencesHolder.loadLong(Constants.USER_ID_PREF);
        String url = Constants.URL_DELETE_ALL_ORDERS;

        //Calling the delete_order.php script and delete the order
        Ion.with(PreviousOrderActivity.this)
                .load(url)
                .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS)
                .setBodyParameter(Constants.TAG_UID, uid + "")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        //Refreshing the listView with new values after deleting order.
                        retrieveOrders();
                    }
                });
    }


}
