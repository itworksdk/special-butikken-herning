package dk.itworks.special_butikken_herning.service;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import dk.itworks.special_butikken_herning.R;
import dk.itworks.special_butikken_herning.ScannerActivity;
import dk.itworks.special_butikken_herning.structure.Vare;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import utils.Constants;


public class VareValidationService extends IntentService {
    //Our trusty Realms
    private Realm realm;

    //Responds string. If this string is OK, the item exists.
    private String responseMessage;

    //Initiating a default service-constructor. The "super" argument MUST contain this class' name.
    public VareValidationService() {
        super("VareValidationService");
    }

    //Method to call the webservice. Will be launched outside of Main thread, but sequently.
    @Override
    protected void onHandleIntent(final Intent intent) {

        //Fetchin the scanned ean-number from ScannerActivity. Will be used in the POST-method
        final String eanNumber = intent.getStringExtra(Constants.INTENT_EAN_NUMBER);

        //Fetching vareID. This ID will be checked if the item is already in the cart.
        final int nextKey = intent.getIntExtra(Constants.INTENT_GET_NEXT_KEY, 0);

        //URL to the webservice
        String url = Constants.URL_VALIDATE_ITEM;

        //Ion initiation
        Ion.with(getApplicationContext())
                .load(url)
                .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS)
                .setBodyParameter(Constants.EAN, eanNumber)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {

                            try {
                                //Initiating Realm if no errors
                                RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(getApplicationContext()).build();
                                realm = Realm.getInstance(realmConfiguration);

                                //Retrieving the JSONArray from the responds
                                JsonArray resArr = result.get(Constants.RESULTS).getAsJsonArray();

                                //Fetching the data. Since it only returns 1 object we look at the 0th index (first result)
                                final int vareId = resArr.get(0).getAsJsonObject().get(Constants.VARE_ID).getAsInt();
                                final String vareName = resArr.get(0).getAsJsonObject().get(Constants.VARE_NAME).getAsString();
                                final double varePrice = resArr.get(0).getAsJsonObject().get(Constants.VARE_PRICE).getAsDouble();
                                final String vareNumber = resArr.get(0).getAsJsonObject().get(Constants.VARE_NUMBER).getAsString();

                                //Retrieving the item where the EAN number matches.
                                final Vare vare = realm.where(Vare.class).equalTo("vareEANNumber", eanNumber).findFirst();

                                //If there is a match (item-list is NOT empty)
                                if (vare != null) {
                                    //Increase the amount of items if a match
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            int amount = vare.getVareAmount() + 1;
                                            double total = varePrice * amount;

                                            vare.setVareAmount(amount);
                                            vare.setVareIdDatabase(vareId+"");
                                            vare.setVareNumber(vareNumber);
                                            vare.setVareTotalPrice(total);
                                            realm.copyToRealmOrUpdate(vare);
                                        }
                                    });

                                    //If no match, insert a new item in Realm.
                                } else {
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            Vare vare = new Vare();
                                            vare.setVareID(nextKey);
                                            vare.setVareIdDatabase(vareId+"");
                                            vare.setVareName(vareName);
                                            vare.setVareNumber(vareNumber);
                                            vare.setVarePrice(varePrice);
                                            vare.setVareEANNumber(eanNumber);
                                            vare.setVareAmount(vare.getVareAmount() + 1);
                                            vare.setVareTotalPrice(varePrice);

                                            realm.copyToRealmOrUpdate(vare);
                                        }
                                    });
                                }

                                //Everything went fine, so set respondsMessage to OK
                                responseMessage = Constants.VARE_SERVICE_OK_RESPONDS;

                            } catch (Exception x) {
                                x.printStackTrace();

                                //Something went wrong, so set respondsMessage to ERROR
                                responseMessage = Constants.VARE_SERVICE_ERROR_RESPONDS;
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error), Toast.LENGTH_LONG).show();

                            }
                            realm.close();

                        } else {
                            e.printStackTrace();
                            //Something went wrong, so set respondsMessage to ERROR
                            responseMessage = Constants.VARE_SERVICE_ERROR_RESPONDS;
                            if (realm != null)
                                realm.close();
                        }
                        // Sending the respondsMessage to the ScannerActivity.
                        // If the response is "OK" the dialog will show
                        // If "ERROR" then the Toast message with error message will be displaye



                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(ScannerActivity.VareBroadcastReceiver.vareBroadcastReceiverResponse);
                        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
                        broadcastIntent.putExtra(Constants.INTENT_SERVICE_ITEM_RESPONDS, responseMessage); //Response message
                        sendBroadcast(broadcastIntent);
                    }
                });


    }


}
