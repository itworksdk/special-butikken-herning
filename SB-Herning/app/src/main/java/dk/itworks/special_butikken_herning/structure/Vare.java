package dk.itworks.special_butikken_herning.structure;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Vare extends RealmObject {
    @PrimaryKey
    private int vareID;

    private String vareIdDatabase;
    private String vareEANNumber;
    private double varePrice;
    private String vareName;
    private String vareNumber;
    private int vareAmount;
    private double vareTotalPrice;

    /*
    Vare information
     */



    public int getVareID() {
        return vareID;
    }

    public String getVareIdDatabase() {
        return vareIdDatabase;
    }

    public void setVareIdDatabase(String vareIdDatabase) {
        this.vareIdDatabase = vareIdDatabase;
    }

    public void setVareID(int vareID) {
        this.vareID = vareID;
    }

    public String getVareEANNumber() {
        return vareEANNumber;
    }

    public void setVareEANNumber(String vareEANNumber) {
        this.vareEANNumber = vareEANNumber;
    }

    public double getVarePrice() {
        return varePrice;
    }

    public void setVarePrice(double varePrice) {
        this.varePrice = varePrice;
    }

    public String getVareName() {
        return vareName;
    }

    public void setVareName(String vareName) {
        this.vareName = vareName;
    }

    public int getVareAmount() {
        return vareAmount;
    }

    public void setVareAmount(int vareAmount) {
        this.vareAmount = vareAmount;
    }

    public String getVareNumber() {
        return vareNumber;
    }

    public void setVareNumber(String vareNumber) {
        this.vareNumber = vareNumber;
    }

    public double getVareTotalPrice() {
        return vareTotalPrice;
    }

    public void setVareTotalPrice(double vareTotalPrice) {
        this.vareTotalPrice = vareTotalPrice;
    }


}
