package dk.itworks.special_butikken_herning.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import dk.itworks.special_butikken_herning.R;
import dk.itworks.special_butikken_herning.fragments.EditVareDialogFragment;
import dk.itworks.special_butikken_herning.structure.Vare;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import utils.Constants;


public class VareRealmAdapter extends RealmBaseAdapter<Vare> implements ListAdapter {
    public VareRealmAdapter(
            Context context,
            RealmResults<Vare> realmResults,
            boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.single_item_vare, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TextView titleTv = viewHolder.single_vare_name;
        TextView priceTv = viewHolder.single_vare_price;
        TextView eanTv = viewHolder.single_vare_ean_nr;

        Vare vare = realmResults.get(position);

        titleTv.setText(vare.getVareName());
        priceTv.setText(
                vare.getVareAmount() + " X " +
                String.format("%.2f", vare.getVarePrice()) + " DKK");
        eanTv.setText(vare.getVareEANNumber());

        return convertView;
    }

    @Override
    public Vare getItem(int i) {
        Vare vare = realmResults.get(i);

        int vareID = vare.getVareID();
        String vareIdDB = vare.getVareIdDatabase();
        String vareNumber = vare.getVareNumber()+"";
        String title = vare.getVareName();
        String price = String.valueOf(vare.getVarePrice());
        String ean = vare.getVareEANNumber();
        int amount = vare.getVareAmount();

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BUNDLE_VARE_ID, vareID);
        bundle.putString(Constants.BUNDLE_VARE_ID_DB, vareIdDB);
        bundle.putString(Constants.BUNDLE_VARE_NUMBER, vareNumber);
        bundle.putString(Constants.BUNDLE_TITLE, title);
        bundle.putString(Constants.BUNDLE_PRICE, price);
        bundle.putString(Constants.BUNDLE_EAN_NUMBER, ean);
        bundle.putInt(Constants.BUNDLE_VARE_AMOUNT, amount);

        DialogFragment fragment = EditVareDialogFragment.newInstance();
        fragment.setArguments(bundle);
        fragment.show(((FragmentActivity) context).getSupportFragmentManager(), "edit_dialog");

        return super.getItem(i);
    }

    static class ViewHolder {
        private TextView single_vare_name, single_vare_price, single_vare_ean_nr;
        public ViewHolder(View view) {
            single_vare_name = (TextView) view.findViewById(R.id.single_vare_name);
            single_vare_price = (TextView) view.findViewById(R.id.single_vare_price);
            single_vare_ean_nr = (TextView) view.findViewById(R.id.single_vare_ean_nr);

        }
    }
}
