package dk.itworks.special_butikken_herning.structure;

/**
 * Created by Harjit on 02/08/15.
 */
public class VareFieldName {
    public static final String vareID = "vareID";
    public static final String vareEANNumber = "vareEANNumber";
    public static final String varePrice = "varePrice";
    public static final String vareName = "vareName";
    public static final String vareNumber = "vareNumber";
    public static final String vareAmount = "vareAmount";
    public static final String vareTotalPrice = "vareTotalPrice";
}
