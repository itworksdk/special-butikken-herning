package dk.itworks.special_butikken_herning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import scanner.MessageDialogFragment;
import utils.CheckInternet;
import utils.Constants;
import utils.HideKeyboard;
import utils.SharedPreferencesHolder;


public class LoginActivity extends AppCompatActivity implements MessageDialogFragment.MessageDialogListener {
    //All required views.
    private EditText username_editext_login;
    private EditText password_edittext_login;
    private Button button_login;
    private ScrollView login_scrollView;
    private ProgressBar login_progressBar;

    private ArrayList<EditText> errorEdit;

    //Defining Realm objects
    private Realm realm;
    private RealmConfiguration realmConfiguration;

    private SharedPreferencesHolder spHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        //Checking if user already has logged in.
        spHolder = new SharedPreferencesHolder(LoginActivity.this);
        loadOnce();

        //Initiating vires
        username_editext_login = (EditText) findViewById(R.id.username_editext_login);
        password_edittext_login = (EditText) findViewById(R.id.password_edittext_login);
        login_scrollView = (ScrollView) findViewById(R.id.login_scrollView);
        login_progressBar = (ProgressBar) findViewById(R.id.login_progressBar);
        button_login = (Button) findViewById(R.id.button_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });

        //If user created a new account, the username will be displayed (improving user experience)
        String uName = getIntent().getStringExtra(Constants.INTENT_GET_USER_NAME_AFTER_CREATE_USER);
        if (uName != null)
            username_editext_login.setText(uName);

        //Save already-typed-in text when screen has rotated.
        if (savedInstanceState != null) {
            username_editext_login.setText(savedInstanceState.getString(Constants.BUNDLE_USER_NAME));
            password_edittext_login.setText(savedInstanceState.getString(Constants.BUNDLE_USER_PASS));
        }

    }

    //Saves a sharedpreference-object to check if user is logged in.
    private void loadOnce() {
        //Loading a boolean to check if user is logged in.

        boolean checkForThisBhenn = spHolder.loadBoolean(Constants.LOAD_LOGINSCREEN_ONCE);

        //Go to MainActivity if user is logged in.
        if (checkForThisBhenn) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        } else {
            //Build the realm-object if user is not logged in.
            realmConfiguration = new RealmConfiguration.Builder(LoginActivity.this).build();
            realm = Realm.getInstance(realmConfiguration);

        }
    }

    //Saving the text in the EditText fields when screen is rotating.
    @Override
    protected void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);

        b.putString(Constants.BUNDLE_USER_NAME, username_editext_login.getText().toString());
        b.putString(Constants.BUNDLE_USER_PASS, password_edittext_login.getText().toString());
    }

    private void validateEditText(EditText[] fields) {
        errorEdit = new ArrayList<>();
        String error = getResources().getString(R.string.text_empty);

        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];

            if (currentField.getText().toString().length() <= 0) {
                currentField.setError(error);
                errorEdit.add(currentField);
            }
        }
    }

    //First validate if all fields are entered. If so, start login().
    private void checkLogin() {
        validateEditText(new EditText[]{
                username_editext_login,
                password_edittext_login
        });

        //If any textfields are added to the array. Display the error
        if (errorEdit.size() > 0) {
            for (int i = 0; i < errorEdit.size(); ++i) {
                errorEdit.get(i);
            }

        } else {
            if (new CheckInternet().isOnline(LoginActivity.this))
                login();
            else {
                //If no internet connection warn the user.
                String title = getResources().getString(R.string.warning);
                String message = getResources().getString(R.string.check_to_internet);

                DialogFragment dialogFragment = MessageDialogFragment.newInstance(title, message, this);
                dialogFragment.show(getSupportFragmentManager(), "no_internet_dialog");
            }
        }
    }

    private void setEditTextError() {
        String error = getResources().getString(R.string.text_empty);
        username_editext_login.setError(error);
        password_edittext_login.setError(error);
    }

    //User is logged in.
    private void login() {
        //Hide softkeyboard for better UX.
        login_scrollView.setVisibility(View.GONE);
        login_progressBar.setVisibility(View.VISIBLE);
        new HideKeyboard(LoginActivity.this).hide(password_edittext_login);

        String url = Constants.URL_LOGIN;

        //Launching an Ion-object.
        Ion.with(LoginActivity.this)
                .load(url)
                .setTimeout(10000) //If no response in more than 10 sec. Show error
                .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS) //User/pass for the server
                .setBodyParameter(Constants.POST_EMAIL, username_editext_login.getText().toString()) //Post params
                .setBodyParameter(Constants.POST_PASS, password_edittext_login.getText().toString())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {

                            String success = result.get(Constants.SUCCESS).getAsString();

                            //If JSON-response is 1, login
                            if (success.equals("1")) {
                                //Getting user info
                                final long uid = result.get(Constants.TAG_UID).getAsLong();
                                final String customerID = result.get(Constants.TAG_CUSTOMER_ID).getAsString();
                                final String userName = result.get(Constants.TAG_NAME).getAsString();

                                //Saving userID as sharedpreference
                                spHolder.saveLong(Constants.USER_ID_PREF, uid);

                                //Saving CustomerID which is used when user orders something
                                spHolder.saveString(Constants.USER_CUSTOMER_ID_PREF, customerID);

                                //Setting SP to "true". Know system knows user is logged.
                                spHolder.saveBoolean(Constants.LOAD_LOGINSCREEN_ONCE, true);

                                //Saving userName as sharedpreference
                                spHolder.saveString(Constants.USER_NAME_PREF, userName);

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();

                            } else {
                                //If success != 1, display an error
                                login_progressBar.setVisibility(View.GONE);
                                login_scrollView.setVisibility(View.VISIBLE);
                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.wrong_credientials), Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            //Something went wrong. Display a message
                            login_progressBar.setVisibility(View.GONE);
                            login_scrollView.setVisibility(View.VISIBLE);
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                            password_edittext_login.setText("");
                        }

                    }
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Close the realm object when leaving the Activity.
        realm.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_item_login) {
            //Go to CreateUserActivty
            startActivity(new Intent(LoginActivity.this, CreateUserActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Dismiss the dialog if user does NOT have internet connection.
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        dialog.dismiss();
    }
}
