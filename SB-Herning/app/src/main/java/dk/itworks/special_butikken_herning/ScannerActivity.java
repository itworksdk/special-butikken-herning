package dk.itworks.special_butikken_herning;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.Button;

import com.google.zxing.Result;

import java.util.ArrayList;

import dk.itworks.special_butikken_herning.service.VareValidationService;
import dk.itworks.special_butikken_herning.structure.Vare;
import dk.itworks.special_butikken_herning.structure.VareFieldName;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import scanner.MessageDialogFragment;
import utils.Constants;
import utils.NotificationIconHandler;
import utils.RealmHelper;


public class ScannerActivity extends AppCompatActivity implements MessageDialogFragment.MessageDialogListener,
        ZXingScannerView.ResultHandler {

    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    private ZXingScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;

    private ProgressDialog progressDialog;
    private static Button notifCount;
    private static int mNotifCount = 0;

    private Realm realm;
    private RealmResults<Vare> results;

    private VareBroadcastReceiver receiver;


    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        if (state != null) {
            mFlash = state.getBoolean(FLASH_STATE, false);
            mAutoFocus = state.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = state.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = state.getInt(CAMERA_ID, -1);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }

        progressDialog = new ProgressDialog(ScannerActivity.this);

        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);

    }


    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);

        // IntentFilter filter = new IntentFilter(Constants.BROADCAST_ACTION);
        IntentFilter filter = new IntentFilter(VareBroadcastReceiver.vareBroadcastReceiverResponse);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new VareBroadcastReceiver();
        registerReceiver(receiver, filter);


        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        realm = Realm.getInstance(realmConfiguration);

        results = realm.where(Vare.class)
                .findAll();

        mNotifCount = results.size();
        setCartValue(mNotifCount);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        NotificationIconHandler notificationIconHandler = new NotificationIconHandler(ScannerActivity.this);
        notificationIconHandler.notificationIcon(menu, notifCount, mNotifCount);
        return super.onCreateOptionsMenu(menu);
    }


    private void setCartValue(int count) {
        mNotifCount = count;
        supportInvalidateOptionsMenu();
    }

    @Override
    public void handleResult(Result rawResult) {
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //showMessageDialog();
        Intent intentValues = new Intent(ScannerActivity.this, VareValidationService.class);
        intentValues.putExtra(Constants.INTENT_GET_NEXT_KEY, new RealmHelper(realm).getNextValue(VareFieldName.vareID));
        intentValues.putExtra(Constants.INTENT_EAN_NUMBER, rawResult.getText());
        startService(intentValues);


    }

    public void showMessageDialog() {
        progressDialog.dismiss();

        String title = getResources().getString(R.string.cart_dialog_title);
        String addedToCart = getResources().getString(R.string.added_to_cart);

        DialogFragment fragment = MessageDialogFragment.newInstance(title, addedToCart, this);
        fragment.show(getSupportFragmentManager(), "scan_results");
        Log.d("showMessageDialog res", results.size() + "");
        setCartValue(results.size());


    }


    public void closeMessageDialog() {
        closeDialog("scan_results");
    }

    public void closeFormatsDialog() {
        closeDialog("format_selector");
    }

    public void closeDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        dialog.dismiss();
    }

    private void resumeCamera() {
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);

        setCartValue(results.size());
    }


    @Override
    public void onPause() {
        super.onPause();

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        mScannerView.stopCamera();
        closeMessageDialog();
        closeFormatsDialog();

        if (receiver != null)
            unregisterReceiver(receiver);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        realm.close();
    }

    public class VareBroadcastReceiver extends BroadcastReceiver {
        public static final String vareBroadcastReceiverResponse = Constants.BROADCAST_ACTION;

        @Override
        public void onReceive(Context context, Intent intent) {

            String responds = intent.getStringExtra(Constants.INTENT_SERVICE_ITEM_RESPONDS);

            if (responds.equals(Constants.VARE_SERVICE_OK_RESPONDS)) {
                setCartValue(results.size());
                showMessageDialog();
            } else
                progressDialog.dismiss();

            resumeCamera();
        }
    }
}