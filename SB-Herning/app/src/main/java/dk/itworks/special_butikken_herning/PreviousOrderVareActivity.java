package dk.itworks.special_butikken_herning;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import dk.itworks.special_butikken_herning.adapter.PreviousVareAdapter;
import utils.Constants;

public class PreviousOrderVareActivity extends AppCompatActivity {
    private ArrayList<HashMap<String, String>> resultVareList;
    private ListView previous_orders_vare_listView;

    private ListAdapter previousOrderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_order_vare);

        //Filling our arraylist with the retrieved items from orders.
        resultVareList = new ArrayList<>();
        resultVareList = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra(Constants.INTENT_PREVIOUS_ORDER_VARER_BUNDLE);

        //Retrieving specific order ID.
        String oidFromIntent = getIntent().getStringExtra(Constants.INTENT_PREVIOUS_ORDER_VARER_BUNDLE_OID);
        ArrayList<HashMap<String, String>> resultVareTemp = new ArrayList<>();

        //Looping over the arrayList and only fetching the results where the oid is.
        for (int i = 0; i < resultVareList.size(); i++) {
            HashMap<String, String> map = resultVareList.get(i);
            String oidFromVare = map.get(Constants.TAG_OID);

            //Only get the list where it oid is.
            if (oidFromVare.equals(oidFromIntent)) {
                resultVareTemp.add(map);
            }
        }

        //Setting the order ID as the title in ActionBar
        String actionBarTitle = getResources().getString(R.string.previous_order_list_item_text) + " " + oidFromIntent;
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(actionBarTitle);

        //Setting adapter for listView
        previous_orders_vare_listView = (ListView) findViewById(R.id.previous_orders_vare_listView);
        previousOrderAdapter = new PreviousVareAdapter(getApplicationContext(), resultVareTemp);
        previous_orders_vare_listView.setAdapter(previousOrderAdapter);
    }


}
