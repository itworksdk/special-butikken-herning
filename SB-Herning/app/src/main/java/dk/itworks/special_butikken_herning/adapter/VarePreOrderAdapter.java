package dk.itworks.special_butikken_herning.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashMap;

import dk.itworks.special_butikken_herning.PreviousOrderActivity;
import dk.itworks.special_butikken_herning.PreviousOrderVareActivity;
import dk.itworks.special_butikken_herning.R;
import scanner.MessageDialogFragment;
import utils.Constants;
import utils.SharedPreferencesHolder;


public class VarePreOrderAdapter extends BaseAdapter {
    //Initiating views
    private FragmentActivity context;
    private ArrayList<HashMap<String, String>> resultArray;
    private LayoutInflater inflater;

    public VarePreOrderAdapter(
            FragmentActivity context,
            ArrayList<HashMap<String, String>> resultArray) {

        //Initiating the values
        this.context = context;
        this.resultArray = resultArray;

        //Initiating LayoutInflater. This inflates the parent-view, and draws the UI for a ListItem.
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Classic ViewHolder framework.
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.single_item_pre_orders, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Declaring textViews to improve readability.
        TextView titleTv = viewHolder.pre_order_title;
        TextView dateTv = viewHolder.pre_order_date;
        ImageView deleteIv = viewHolder.pre_order_delete;

        //Initiating our HashMap object in relation to ArrayList.
        HashMap<String, String> map = resultArray.get(position);

        final String titleOid = map.get(Constants.TAG_OID);
        final String timeStamp = map.get(Constants.TAG_TIME_STAMP);

        //Setting fetched data from the resultArray into the TextViews.
        titleTv.setText(context.getResources().getString(R.string.previous_order_list_item_text)
                + "\t" + titleOid);
        dateTv.setText(timeStamp);
        deleteIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Standard dialogfragment handling
                String title = context.getResources().getString(R.string.delete_title);
                String addedToCart = context.getResources().getString(R.string.delete_message);

                final long uid = new SharedPreferencesHolder(context).loadLong(Constants.USER_ID_PREF);

                //Inserting the bundle containing the order ID and launching the dialog
                DialogFragment fragment = MessageDialogFragment.newInstance(title, addedToCart, new MessageDialogFragment.MessageDialogListener() {
                    @Override
                    public void onDialogPositiveClick(DialogFragment dialog) {
                        //Calling the delete_order.php script and delete the order
                        String url = Constants.URL_DELETE_ORDER;
                        Ion.with(context)
                                .load(url)
                                .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS)
                                .setBodyParameter(Constants.TAG_OID, titleOid)
                                .setBodyParameter(Constants.TAG_UID, uid+"")
                                .asString()
                                .setCallback(new FutureCallback<String>() {
                                    @Override
                                    public void onCompleted(Exception e, String result) {
                                        context.startActivity(new Intent(context, PreviousOrderActivity.class));
                                        context.finish();
                                    }
                                });

                    }
                });
                fragment.show(context.getSupportFragmentManager(), "delete_pre_order_dialog");


            }
        });


        return convertView;
    }


    @Override
    public int getCount() {
        return resultArray.size();
    }

    @Override
    public Object getItem(int position) {
        HashMap<String, String> map = resultArray.get(position);
        String oid = map.get(Constants.TAG_OID);

        Intent intent = new Intent(context, PreviousOrderVareActivity.class);
        intent.putExtra(Constants.INTENT_PREVIOUS_ORDER_VARER_OID, oid);


        return resultArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    //Classic ViewHolder Framework.
    static class ViewHolder {
        private TextView pre_order_title, pre_order_date;
        private ImageView pre_order_delete;

        public ViewHolder(View view) {
            pre_order_title = (TextView) view.findViewById(R.id.pre_order_title);
            pre_order_date = (TextView) view.findViewById(R.id.pre_order_date);
            pre_order_delete = (ImageView) view.findViewById(R.id.pre_order_delete);

        }
    }
}
