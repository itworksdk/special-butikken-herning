package dk.itworks.special_butikken_herning;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import dk.itworks.special_butikken_herning.adapter.VareRealmAdapter;
import dk.itworks.special_butikken_herning.service.UploadFile;
import dk.itworks.special_butikken_herning.structure.Vare;
import dk.itworks.special_butikken_herning.structure.VareFieldName;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import scanner.MessageDialogFragment;
import utils.Constants;
import utils.RealmHelper;
import utils.SharedPreferencesHolder;


public class VareActivity extends AppCompatActivity implements MessageDialogFragment.MessageDialogListener {
    //Defining views
    private Button vare_order_button;
    public TextView vare_total_price;
    public TextView empty_view_vare_list;
    private ListView vare_listView;
    private ListAdapter adapter;

    //Defining Realms
    private RealmConfiguration realmConfiguration;
    private Realm realm;

    //Defining StringBuilder
    private StringBuilder b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vare);

        //Initiating realm
        realmConfiguration = new RealmConfiguration.Builder(getApplicationContext()).build();
        realm = Realm.getInstance(realmConfiguration);

        //Intiating views
        empty_view_vare_list = (TextView) findViewById(R.id.empty_view_vare_list);
        vare_total_price = (TextView) findViewById(R.id.vare_total_price);
        vare_listView = (ListView) findViewById(R.id.vare_listView);
        vare_listView.setEmptyView(empty_view_vare_list);
        vare_order_button = (Button) findViewById(R.id.vare_order_button);
        vare_order_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new RealmHelper(VareActivity.this).exportDatabase();

                RealmResults<Vare> realmResults = realm.where(Vare.class).findAll();

                if (realmResults.size() != 0) {
                    //Call a dialog if user presses order butotn
                    DialogFragment fragment = MessageDialogFragment.newInstance(
                            getResources().getString(R.string.order_title),
                            getResources().getString(R.string.order_message),
                            VareActivity.this);
                    fragment.show(getSupportFragmentManager(), "order_fragment");
                } else {
                    //Display error if no items in cart.
                    Toast.makeText(VareActivity.this, getResources().getString(R.string.no_items), Toast.LENGTH_SHORT).show();
                }


            }
        });

        //Insert test items in the cart.
        // new RealmHelper(realm).insertRealTestResults(5);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Stop remaining Realms

    }

    private void initProducts() {
        RealmResults<Vare> realmResults = realm.where(Vare.class).findAll();
        adapter = new VareRealmAdapter(VareActivity.this, realmResults, true);
        vare_listView.setAdapter(adapter);
        vare_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.getItem(position);
            }
        });
        double price = realmResults.sum(VareFieldName.vareTotalPrice).doubleValue();
        vare_total_price.setText(String.format("%.2f", price) + " DKK");
    }

    //Method to refresh the item-listview
    private void refreshRealmResults() {
        Vare vare = realm.allObjects(Vare.class).first();
        if (vare.isValid()) {
            initProducts();

        } else {
            //Initiating realm
            realmConfiguration = new RealmConfiguration.Builder(getApplicationContext()).build();
            realm = Realm.getInstance(realmConfiguration);
            initProducts();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshRealmResults();
    }


    //Helper method that takes a String, and adds quotes
    private String wrapResult(String result) {
        return "\"" + result + "\"";
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        //Here's our items in a list
        final RealmResults<Vare> results = realm.where(Vare.class).findAll();

        //Getting CustomerID from sharedPreferences (We got this in the LoginActivity)
        final String customerID = new SharedPreferencesHolder(VareActivity.this).loadString(Constants.USER_CUSTOMER_ID_PREF);

        //Cycling through all the orders, and putting them in the correct format:

        //Customer_id;"Price";"Produkt Name";"EAN number;Produkt Number";"Quantity" (WITHOUT LAST SEMICOLON, AND NO QUOTES AROUND Customer_id)

        //Initating our StringBuilder used for appending all our items to 1 string. We will send this String to the text-file.
        b = new StringBuilder();
        for (Vare vare : results) {

            double price = vare.getVarePrice() * 10;

            String str = "\r\n";

            b
                    .append(wrapResult(customerID))
                    .append(";")
                            //.append(wrapResult(String.valueOf(vare.getVarePrice()).replace(".", "")))
                    .append(wrapResult(String.valueOf(price).replace(".", "")))
                    .append(";")
                    .append(wrapResult(vare.getVareName()))
                    .append(";")
                    .append(wrapResult(vare.getVareEANNumber()))
                    .append(";")
                    .append(wrapResult(vare.getVareNumber()))
                    .append(";")
                    .append(wrapResult(String.valueOf(vare.getVareAmount())))
                            // .append("\r\n");
                    .append("\n");

            //97122599;"13990";"BRYNJE SIKKERHEDSST?VLE COOL P";"5705568021478";"365";"2"
        }
        b.setLength(b.length() - 1); //Removes last "new-line" the String.

        upload(); //Uploading file.
        //ion();
        //showMessageDialog();

        /*
        Intent intentValues = new Intent(this, dk.itworks.special_butikken_herning.service.Upload.class);
        this.startService(intentValues);
        Log.d("here","here");
        */
    }

    //Writes the order into a file. The file will be uploaded to the server.
    private void writeToFile(String file, String data) {

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ion() {
        String fileName = "sb_herning" + ".txt";
        File resultFile = new File(Environment.getExternalStorageDirectory(), fileName);

        writeToFile(resultFile.getAbsoluteFile().toString(), b.toString());

        Ion.with(getApplicationContext())
                .load("POST", Constants.URL_UPLOAD_ORDER)
                .uploadProgressHandler(new ProgressCallback() {
                    @Override
                    public void onProgress(long uploaded, long total) {
                        // Displays the progress bar for the first time.
                        //mNotifyManager.notify(notificationId, mBuilder.build());
                        //mBuilder.setProgress((int) total, (int) uploaded, false);
                    }
                })
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("upload.txt", "image/jpeg", resultFile)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e != null) {
                            Toast.makeText(VareActivity.this, "error uploading", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            return;
                        }
                        Log.d("result", result);
                        Toast.makeText(VareActivity.this, "File upload complete", Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void upload() {
        //Getting current date-time.
        @SuppressLint("SimpleDateFormat")
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = df.format(Calendar.getInstance().getTime());

        //Get customer ID
        String customerID = new SharedPreferencesHolder(VareActivity.this).loadString(Constants.USER_CUSTOMER_ID_PREF);

        //File name with current date-time
        String fileName = "sb_herning_" + customerID + "_" + date + ".txt";
        File resultFile = new File(Environment.getExternalStorageDirectory(), fileName);


        //If file on phone does not exists, create a new, insert the StringBuilder and upload it.
        if (!resultFile.exists()) {
            writeToFile(resultFile.getAbsoluteFile().toString(), b.toString());
            new Upload().execute(resultFile);
        }

    }

    private String buildUrl() {

        RealmResults<Vare> realmResults = realm.where(Vare.class).findAll();
        StringBuilder builder = new StringBuilder();

        builder.append("[");

        for (Vare vare : realmResults) {
            String vareID = vare.getVareIdDatabase() + "";
            String amount = vare.getVareAmount() + "";

            builder.append("[" + vareID + "," + amount + "],");

        }

        builder.setLength(builder.length() - 1);
        builder.append("]");

        return builder.toString();
    }

    private ProgressDialog progressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(VareActivity.this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.show();
        return progressDialog;
    }


    private class Upload extends AsyncTask<File, Void, String> {
        private ProgressDialog progressDialog;
        String urlFromBuilder = buildUrl();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Initiating a loadingDialog
            progressDialog = new ProgressDialog(VareActivity.this);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.show();


        }

        @Override
        protected String doInBackground(File... file) {
            new UploadFile(
                    Constants.URL_UPLOAD_ORDER,
                    file[0].getAbsolutePath(),
                    VareActivity.this
            ).upload(
                    Constants.SERVER_USERNAME,
                    Constants.SERVER_PASS
            );
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            //Show dialog after purchasing
            finishPurchasing();

            //Inserting order into Database
            long uid = new SharedPreferencesHolder(VareActivity.this).loadLong(Constants.USER_ID_PREF);

            String url = Constants.URL_INSERT_ORDER + "?" + Constants.TAG_UID + "=" + uid + "&" + Constants.TAG_NEW_ORDER + "=" + urlFromBuilder; //result that is encoded.

            Log.d("url insert into db", url);

            Ion.with(VareActivity.this)
                    .load(url)
                    .basicAuthentication(Constants.SERVER_USERNAME, Constants.SERVER_PASS)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            //Clearing the cart.
                            new RealmHelper(realm).clearVareRealm();
                        }
                    });

            Log.d("urlFromBuilder", urlFromBuilder);
        }

    }

    private void finishPurchasing() {
        String title = getResources().getString(R.string.title_dialog_purchased_item);
        String message = getResources().getString(R.string.message_dialog_purchased_item);

        DialogFragment dialog = MessageDialogFragment.newInstance(title, message, new MessageDialogFragment.MessageDialogListener() {
            @Override
            public void onDialogPositiveClick(DialogFragment dialog) {
                Intent intent = new Intent(VareActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), "finish_purchasing_dialogfragment");
    }
}