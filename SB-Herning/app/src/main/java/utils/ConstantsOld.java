package utils;

public class ConstantsOld {
    //Server info
    public static final String SERVER_USERNAME = "server15";
    public static final String SERVER_PASS = "server15";

    //FTP info
    public static final String URL_UPLOAD_ORDER = "http://apptest.dk.server15.exaweb.dk/sb/orders/upload_order.php";

    //Login and user info
    public static final String URL_LOGIN = "http://apptest.dk.server15.exaweb.dk/sb/login.php";
    public static final String POST_EMAIL = "email";
    public static final String POST_PASS = "password";
    public static final String SUCCESS = "success";
    public static final String TAG_UID = "uid";
    public static final String TAG_NAME = "name";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_NEW_ORDER = "neworder";

    public static final String URL_CREATE_USER = "http://apptest.dk.server15.exaweb.dk/sb/create_user.php";

    //Varer
    public static final String URL_VALIDATE_ITEM = "http://apptest.dk.server15.exaweb.dk/sb/validate_item.php";
    public static final String RESULTS = "results";
    public static final String VARE_ID = "vare_id";
    public static final String VARE_NAME = "vare_name";
    public static final String VARE_PRICE = "vare_price";
    public static final String VARE_EAN_NUMBER = "ean_number";
    public static final String VARE_NUMBER = "vare_number";
    public static final String EAN = "ean";
    public static final String VARER = "varer";
    public static final String AMOUNT = "amount";

    //VareActivity
    public static final String URL_INSERT_ORDER = "http://apptest.dk.server15.exaweb.dk/sb/multi_order_submit.php";
    public static final String URL_FETCH_LATEST_ORDER_ID = "http://apptest.dk.server15.exaweb.dk/sb/get_latest_order_id.php";

    public static final String INTENT_EAN_NUMBER = "INTENT_EAN_NUMBER";
    public static final String INTENT_GET_NEXT_KEY = "INTENT_GET_NEXT_KEY";
    public static final String INTENT_GET_USER_NAME_AFTER_CREATE_USER = "INTENT_GET_USER_NAME_AFTER_CREATE_USER";

    public static final String BUNDLE_VARE_ID = "BUNDLE_VARE_ID";
    public static final String BUNDLE_VARE_ID_DB = "BUNDLE_VARE_ID_DB";
    public static final String BUNDLE_VARE_NUMBER = "BUNDLE_VARE_NUMBER";
    public static final String BUNDLE_TITLE = "BUNDLE_TITLE";
    public static final String BUNDLE_PRICE = "BUNDLE_PRICE";
    public static final String BUNDLE_VARE_AMOUNT = "BUNDLE_VARE_AMOUNT";
    public static final String BUNDLE_EAN_NUMBER = "BUNDLE_EAN_NUMBER";

    //Sharedpreferences
    public static final String USER_ID_PREF = "USER_ID_PREF";
    public static final String LOAD_LOGINSCREEN_ONCE = "LOAD_LOGINSCREEN_ONCE";

    //Bundle login screen
    public static final String BUNDLE_USER_NAME = "BUNDLE_USER_NAME";
    public static final String BUNDLE_USER_PASS = "BUNDLE_USER_PASS";

    //Bundle create new user screen
    public static final String BUNDLE_CREATE_USER_NAME = "BUNDLE_CREATE_USER_NAME";
    public static final String BUNDLE_CREATE_USER_PASS = "BUNDLE_CREATE_USER_PASS";
    public static final String BUNDLE_CREATE_USER_PASS2 = "BUNDLE_CREATE_USER_PASS2";
    public static final String BUNDLE_CREATE_USER_EMAIL = "BUNDLE_CREATE_USER_EMAIL";

    //SB Website
    public static final String URL_SB_WEBSITE = "http://www.sbherning.dk/";

    //Advertisement websites
    public static final String URL_3M_WEBSITE = "http://solutions.3mdanmark.dk/wps/portal/3M/da_DK/EU2/Country/";
    public static final String URL_BAHCO_WEBSITE = "http://www.bahco.com/";
    public static final String URL_METABO_WEBSITE = "http://www.metabo.dk/";

    //Broadcast receiver info
    public static final String INTENT_SERVICE_ITEM_RESPONDS = "INTENT_SERVICE_ITEM_RESPONDS";
    public static final String BROADCAST_ACTION = "dk.itworks.sb_herning.service.VareValidationService";
    public static final String VARE_SERVICE_OK_RESPONDS = "OK";
    public static final String VARE_SERVICE_ERROR_RESPONDS = "ERROR";

    //Previous orders
    public static final String URL_PREVIOUS_ORDERS = "http://apptest.dk.server15.exaweb.dk/sb/multi_order_history.php";
    public static final String URL_DELETE_ORDER = "http://apptest.dk.server15.exaweb.dk/sb/delete_order.php";
    public static final String URL_DELETE_ALL_ORDERS = "http://apptest.dk.server15.exaweb.dk/sb/delete_all_orders.php";
    public static final String INTENT_PREVIOUS_ORDER_VARER_OID = "INTENT_PREVIOUS_ORDER_VARER";
    public static final String INTENT_PREVIOUS_ORDER_VARER_BUNDLE = "INTENT_PREVIOUS_ORDER_VARER_BUNDLE";
    public static final String INTENT_PREVIOUS_ORDER_VARER_BUNDLE_OID = "INTENT_PREVIOUS_ORDER_VARER_BUNDLE_OID";


    //JSON result
    public static final String TAG_ORDERS = "orders";
    public static final String TAG_OID = "oid";
    public static final String TAG_TIME_STAMP = "time_stamp";
}
