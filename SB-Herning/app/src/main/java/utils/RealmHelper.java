package utils;

import dk.itworks.special_butikken_herning.structure.Vare;
import dk.itworks.special_butikken_herning.structure.VareFieldName;
import io.realm.Realm;


public class RealmHelper {
    private Realm realm;

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    //Getting nextvalue of a field. We only need to specify the field name
    public int getNextValue(String field) {
        return (int) realm.where(Vare.class).maximumInt(field) + 1;
    }

    //Clearing all Realms
    public void clearVareRealm() {
        realm.beginTransaction();
        realm.where(Vare.class).findAll().clear();
        realm.commitTransaction();
    }

    //Method to insert X amount of items in the basket.
    public void insertTestResults(int amountOfTestData) {
        for (int i = 1; i < amountOfTestData; i++) {
            final int amount = i;

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Vare vare = new Vare();
                    vare.setVareID(new RealmHelper(realm).getNextValue(VareFieldName.vareID));
                    vare.setVareIdDatabase(amount + "");
                    vare.setVareName("Vare navn " + amount);
                    vare.setVareNumber("AM3095-" + 44 + amount + "");
                    vare.setVarePrice(199.95 + amount);
                    vare.setVareEANNumber("5072391823" + amount);
                    vare.setVareAmount(vare.getVareAmount() + amount);
                    vare.setVareTotalPrice(91 + amount);

                    realm.copyToRealmOrUpdate(vare);
                }
            });
        }
    }

    //Method to insert X amount of items in the basket.
    public void insertRealTestResults(int amountOfTestData) {
        for (int i = 1; i < amountOfTestData; i++) {
            final int amount = i;

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Vare vare = new Vare();
                    vare.setVareID(new RealmHelper(realm).getNextValue(VareFieldName.vareID));
                    vare.setVareIdDatabase(amount + "");
                    vare.setVareName("KLEMR?KKER 12-POL");
                    vare.setVareNumber("9-661-b");
                    vare.setVarePrice(16);
                    vare.setVareEANNumber("5730800523724");
                    vare.setVareAmount(1);
                    vare.setVareTotalPrice(91 + amount);

                    realm.copyToRealmOrUpdate(vare);
                }
            });
        }
    }

/*
    public void exportDatabase() {

        // init realm
        Realm realm = Realm.getInstance(context);

        File exportRealmFile = null;
        try {
            // get or create an "export.realm" file
            exportRealmFile = new File(context.getExternalCacheDir(), "export.realm");

            // if "export.realm" already exists, delete
            exportRealmFile.delete();

            // copy current realm to "export.realm"
            realm.writeCopyTo(exportRealmFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
        realm.close();

        // init email intent and add export.realm as attachment
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, "hs@it-works.dk");
        intent.putExtra(Intent.EXTRA_SUBJECT, "YOUR SUBJECT");
        intent.putExtra(Intent.EXTRA_TEXT, "YOUR TEXT");
        Uri u = Uri.fromFile(exportRealmFile);
        intent.putExtra(Intent.EXTRA_STREAM, u);

        // start email intent
        context.startActivity(Intent.createChooser(intent, "YOUR CHOOSER TITLE"));
    }
    */
}
