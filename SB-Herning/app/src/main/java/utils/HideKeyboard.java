package utils;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Harjit on 02/08/15.
 */
public class HideKeyboard {
    private Context context;

    public HideKeyboard(Context context) {
        this.context = context;

    }

    public void hide(EditText input) {
        input.setInputType(129); //129 = makes the editext as a password field.
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

}
