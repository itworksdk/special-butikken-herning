package utils;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import dk.itworks.special_butikken_herning.R;
import dk.itworks.special_butikken_herning.VareActivity;

public class NotificationIconHandler {
    private Activity context;

    public NotificationIconHandler(Activity context) {
        this.context = context;
    }

    //Building notification icon and functions
    public void notificationIcon(Menu menu, Button notifCount, int mNotifCount) {
        MenuInflater inflater = context.getMenuInflater();
        menu.clear();
        inflater.inflate(R.menu.menu_scanner, menu);

        MenuItem item = menu.findItem(R.id.menu_item_cart);

        MenuItemCompat.setActionView(item, R.layout.feed_update_count);
        View view = MenuItemCompat.getActionView(item);
        notifCount = (Button) view.findViewById(R.id.notif_count);
        notifCount.setText(String.valueOf(mNotifCount));
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VareActivity.class);
                context.startActivity(intent);
            }
        });
    }
}
